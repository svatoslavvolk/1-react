import "./main.scss";
import CardsContainer from "./components/CardsContainer/CardsContainer";
import Header from "./components/Header/Header";
import {useContext, useEffect} from "react";
import { Route, Routes } from "react-router-dom";
import FavoritePage from "./Pages/FavoritePage/FavoritePage";
import CartPage from "./Pages/CartPage/CartPage";
import Modal from "./components/Modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import {
  addItemToCart,
  deleteFromCart,
  getProducts,
  loadCartProducts,
  loadFavoriteProducts,
  toggleFavorites,
  setIsModalOpen,
} from "./store/products/action-creators";

function App() {
  const dispatch = useDispatch();




  const { cart, isModalOpen, modalActionType, selectedProduct, favorites } =
    useSelector((state) => state.products);

  useEffect(() => {
    dispatch(getProducts());
    dispatch(loadCartProducts());
    dispatch(loadFavoriteProducts());
  }, [dispatch]);

  const addToFavorite = (product) => {
    dispatch(toggleFavorites(product.article));
  };

  const handleCloseModal = () => {
    dispatch(setIsModalOpen(false));
  };

  const handleModalConfirm = () => {
    if (modalActionType === "add") {
      dispatch(addItemToCart(selectedProduct.article));
    } else if (modalActionType === "delete") {
      dispatch(deleteFromCart(selectedProduct.article));
    }
  };

  const cartItemsCount = cart.reduce((acc, product) => acc + product.count, 0);
  const favoritesItemsCount = favorites.reduce(
    (acc, product) => acc + product.count,
    0
  );

  return (
    <>
      <Header
        title="SviatoTechnik"
        favoritesCount={favoritesItemsCount}
        cartItemsCount={cartItemsCount}
      />
      <div className="main-wrapper">
        <Routes>
          <Route
            path="/favorites"
            element={<FavoritePage fetchData={"data.json"} />}
          />
          <Route path="/cart" element={<CartPage fetchData={"data.json"} />} />
          <Route
            path="/"
            element={
              <CardsContainer
                addToFavorites={addToFavorite}
                storedFavorites={favorites}
              />
            }
          />
        </Routes>
        {isModalOpen && (
          <Modal
            actionType={modalActionType}
            product={selectedProduct}
            onConfirm={handleModalConfirm}
            onClose={handleCloseModal}
          />
        )}
      </div>
    </>
  );
}

export default App;
