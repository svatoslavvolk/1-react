import CardsInCart from "../../components/Cart/CardsInCart";

function CartPage({ addToFavorites, fetchData, setCart, cart, favorites }) {
    return (
        <>
            {cart && cart.length === 0 ? (
                <h2 className="pages-text">No Items in Cart</h2>
            ) : null}

            <CardsInCart
                setCart={setCart}
                cart={cart}
                addToFavorites={addToFavorites}
                fetchData={fetchData}
                isCartPage={true}
                storedFavorites={favorites}
            />
        </>
    );
}

export default CartPage;
