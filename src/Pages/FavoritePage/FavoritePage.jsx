import FavoriteCards from "../../components/Favorite/FavoriteCards";

function FavoritePage({ handleAddToCart, addToFavorites, fetchData, favorites }) {
    return (
        <>
            {favorites && favorites.length === 0 ? (
                <h2 className="pages-text">No Favorites</h2>
            ) : null}

            <FavoriteCards
                addToFavorites={addToFavorites}
                handleAddToCart={handleAddToCart}
                fetchData={fetchData}
                storedFavorites={favorites}
            />
        </>
    );
}

export default FavoritePage;
