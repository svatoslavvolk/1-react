import PropTypes from "prop-types";
import React from 'react';


function Button ({ text, onClick, color, className}) {
    return (
        <button className={className}  style = {{ color: color}} onClick = {onClick}>
             {text}
        </button>
    )
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    backgroundColor: "#000"
}

export default  Button;