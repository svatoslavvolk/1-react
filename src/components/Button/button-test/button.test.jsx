
import Button from '../Button';
import React from "react";
import { render } from '@testing-library/react';


describe('Button component', () => {
    it('renders without crashing', () => {
        const wrapper = render(<Button text="open-btn" onClick={() => {}} />);
        expect(wrapper.getByText('open-btn')).not.toBeNull();
    });

    it('renders with given text', () => {
        const wrapper = render(<Button text="click me" onClick={() => {}} />);
        expect(wrapper.getByText('click me')).not.toBeNull();
    });

    it('renders correctly with snapshot', () => {
        const wrapper = render(<Button text="snapshot test" onClick={() => {}} />);
        expect(wrapper).toMatchSnapshot();
    });
});

