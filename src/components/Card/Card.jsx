import React, { useContext } from "react";
import Button from "../Button/Button";
import FavoriteAtCard from "../Favorite/FavoriteAtCard";
import PropTypes from "prop-types";
import { toggleFavorites } from "../../store/products/action-creators";
import { useDispatch, useSelector } from "react-redux";
import { Context } from "../../context/viewMode/context";

function Card({
  price,
  name,
  url,
  article,
  color,
  onAddToCart,
  IsModalOpen,
  handleRemoveClick,
  children,
  isCartPage = false,
}) {
  const handleClick = () => {
    dispatch(toggleFavorites(article));
  };

  const dispatch = useDispatch();
  const favorites = useSelector((state) => state.products.favorites);

  const context = useContext(Context);

  const isSelected = favorites.find(
    (favoriteProduct) => favoriteProduct.article === article
  );

  return (
    <>
      <div className={`card-wrapper ${context.mode}`}>
        <img className="card-img" src={url} alt={name} />
        <div>
          <h1 className="card-name">{name}</h1>
          <p className="card-price">Price: {price}</p>
          <p className="card-article">Article: {article}</p>
          <p className="item-color">Color: {color}</p>
        </div>
        <div className={"action-wrapper"}>
          {!isCartPage && (
            <Button
              text="Add to Cart"
              className="open-btn"
              backgroundColor="#9494da"
              onClick={onAddToCart}
            />
          )}
          {children}
          {isCartPage && (
            <div className="cart__button-container">
              <Button
                text="Remove from Cart"
                backgroundColor="#9494da"
                className="delete-cartItem"
                onClick={handleRemoveClick}
              />
            </div>
          )}
          <FavoriteAtCard
            handleClick={handleClick}
            className={`fav-card ${isSelected ? "fav__card-svg" : ""}`}
          />
        </div>
      </div>
    </>
  );
}

Card.propTypes = {
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  article: PropTypes.string.isRequired,
  color: PropTypes.string,
};

Card.defaultProps = {
  color: "",
};

export default Card;
