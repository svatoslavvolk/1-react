import Card from '../Card/Card';
import PropTypes from "prop-types";
import {useDispatch, useSelector} from "react-redux";
import { setIsModalOpen, setModalActionType, setSelectedProduct } from '../../store/products/action-creators';
import {useContext} from "react";
import {Context} from "../../context/viewMode/context";


function CardsContainer({ addToFavorites }) {
    const dispatch = useDispatch();
    const {products, favorites} = useSelector(state  => state.products)

    const handleAddToCart = (product) => {
        dispatch(setIsModalOpen(true));
        dispatch(setModalActionType('add'))
        dispatch(setSelectedProduct(product))
    }

    const context = useContext(Context)

    return (
        <div className={`сards-container ${context.mode}`}>
            {products && products.map(product =>
                <Card
                    key={product.article}
                    color={product.color}
                    price={product.price}
                    name={product.name}
                    url={product.url}
                    addToFavorites={() => addToFavorites(product)}
                    article={product.article}
                    onAddToCart={() => handleAddToCart(product)}
                    isSelected={favorites.find(favoriteProduct =>  favoriteProduct.article === product.article)}
                />
            )}
        </div>
    );
}

export default CardsContainer;

CardsContainer.propTypes = {
    addToFavorites: PropTypes.func.isRequired
}



