import Card from "../Card/Card";
import { useDispatch, useSelector } from "react-redux";

import {
  setIsModalOpen,
  setModalActionType,
  setSelectedProduct,
} from "../../store/products/action-creators";

import React, {useContext, useState} from "react";
import Button from "../Button/Button";
import CheckoutModal from "../CheckoutModal/CheckoutModal";
import {Context} from "../../context/viewMode/context";

function CardsInCart({ addToFavorites, isCartPage }) {
  const [isCheckoutModalOpen, setIsCheckoutModalOpen] = useState(false);

  const { cart, favorites } = useSelector((state) => state.products);

  const dispatch = useDispatch();

  function handleRemoveClick(product) {
    dispatch(setSelectedProduct(product));
    dispatch(setIsModalOpen(true));
    dispatch(setModalActionType("delete"));
  }

  const openCheckoutModal = () => {
    setIsCheckoutModalOpen(true);
  };

  const closeCheckoutModal = () => {
    setIsCheckoutModalOpen(false);
  };

  const context = useContext(Context)

  return (
      <>
        {cart.length === 0 && <h2 className={"cart-title"}>No items in cart.</h2>}

        {cart.length > 0 && (
            <>
              <Button className="buy-btn" text="BUY" onClick={openCheckoutModal} />
              <div className={`сards-container ${context.mode}`}>
                {cart.map((product) => (
                    <Card
                        key={product.article}
                        color={product.color}
                        price={product.price}
                        name={product.name}
                        url={product.url}
                        addToFavorites={() => {
                          addToFavorites(product);
                        }}
                        article={product.article}
                        isCartPage={isCartPage}
                        handleRemoveClick={() => handleRemoveClick(product)}
                        isSelected={favorites.find(
                            (favoriteProduct) => favoriteProduct.article === product.article
                        )}
                    >
                      <span className="quant-cartItem">Quantity: {product.count}</span>
                    </Card>
                ))}
              </div>
            </>
        )}

        {isCheckoutModalOpen && <CheckoutModal onClose={closeCheckoutModal} />}
      </>
  );
}

export default CardsInCart;
