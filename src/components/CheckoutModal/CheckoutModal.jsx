import { Field, Form, Formik, ErrorMessage } from "formik";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { clearCart } from "../../store/products/action-creators";
import * as yup from "yup";
import { PatternFormat } from 'react-number-format';

function CheckoutModal({ onClose }) {
    const cart = useSelector((state) => state.products.cart);
    const dispatch = useDispatch();

    const validationSchema = yup.object().shape({
        email: yup.string().required('Required').email('Invalid email address'),
        password: yup.string().min(6).required('Required'),
        age: yup.number().min(14).max(100).required('Required'),
        name: yup.string().required('Required'),
        surname: yup.string().required('Required'),
        delivery: yup.string().required('Required'),
        phone: yup.string().required('Required').matches(/^\+380 \d{2}-\d{3}-\d{4}$/, 'Invalid phone number')
    });

    const handleWrapperClick = (e) => {
        if (e.target === e.currentTarget) {
            onClose();
        }
    };

    const handleSubmit = (values) => {
        console.log("user", values);
        console.table(cart);
        dispatch(clearCart());
        onClose();
    };

    return (
        <div className="modal-wrapper" onClick={handleWrapperClick}>
            <div className="checkout-modal">
                <p className="checkout__modal-text">Checkout</p>
                <Formik
                    initialValues={{
                        email: "",
                        password: "",
                        age: 0,
                        delivery: "",
                        name: "",
                        surname: "",
                        phone: ""
                    }}
                    validationSchema={validationSchema}
                    onSubmit={handleSubmit}
                >
                    {({ touched, errors, setFieldValue }) => (
                        <Form className="checkout-form">
                            <Field placeholder={"Enter your name"} className={"checkout-field"} name="name" type="text" />
                            {touched.name && errors.name && <ErrorMessage name="name" component="div" className="error-message" />}

                            <Field placeholder={"Enter your surname"} className={"checkout-field"} name="surname" type="text" />
                            {touched.surname && errors.surname && <ErrorMessage name="surname" component="div" className="error-message" />}

                            <Field placeholder={"Enter your age"} className={"checkout-field"} min="0" required name="age" type="number" />
                            {touched.age && errors.age && <ErrorMessage name="age" component="div" className="error-message" />}

                            <Field placeholder={"Enter your delivery address"} className={"checkout-field"} name="delivery" type="text" />
                            {touched.delivery && errors.delivery && <ErrorMessage name="delivery" component="div" className="error-message" />}

                            <Field placeholder={"Enter your password"} className={"checkout-field"} name="password" type="password" />
                            {touched.password && errors.password && <ErrorMessage name="password" component="div" className="error-message" />}

                            <Field placeholder={"Enter your email"} className={"checkout-field"} name="email" type="email" />
                            {touched.email && errors.email && <ErrorMessage name="email" component="div" className="error-message" />}

                            <PatternFormat
                                format="+380 ##-###-####"
                                valueIsNumericString={true}
                                placeholder={"Enter your phone number"}
                                className={"checkout-field"}
                                name="phone"
                                onValueChange={({ formattedValue }) => {
                                    setFieldValue('phone', formattedValue);
                                }}
                            />
                            {touched.phone && errors.phone && <ErrorMessage name="phone" component="div" className="error-message" />}

                            <button className="submit-checkout" type="submit">Buy</button>
                        </Form>
                    )}
                </Formik>

                <button onClick={onClose} className="checkout-close-btn">
                    Close
                </button>
            </div>
        </div>
    );
}

CheckoutModal.propTypes = {
    onClose: PropTypes.func.isRequired,
};

export default CheckoutModal;
