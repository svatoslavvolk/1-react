import Card from '../Card/Card';
import { setIsModalOpen, setModalActionType, setSelectedProduct} from "../../store/products/action-creators";
import {useDispatch, useSelector} from "react-redux";
import React, {useContext} from "react";
import {Context} from "../../context/viewMode/context";

function FavoriteCards({ storedFavorites }) {

    const dispatch = useDispatch();
    const favorites = useSelector(state  => state.products.favorites)

    const handleAddToCart = (product) => {
        dispatch(setIsModalOpen(true));
        dispatch(setModalActionType('add'))
        dispatch(setSelectedProduct(product))
    }

    const context = useContext(Context)

    return (
        <>
            {favorites.length === 0 && <h2 className={"cart-title"}>No items in Favorites.</h2>}
            <div className={`сards-container ${context.mode}`}>
            {favorites && favorites.map(product =>
                <Card
                    key={product.article}
                    color={product.color}
                    price={product.price}
                    name={product.name}
                    url={product.url}
                    article={product.article}
                    onAddToCart={() => handleAddToCart(product)}
                />
            )}
        </div>
        </>
    );
}

export default FavoriteCards;
