import Cart from "../Cart/Cart";
import Favorite from "../Favorite/Favorite";
import PropTypes from "prop-types";
import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import Button from "../Button/Button";
import { Context } from "../../context/viewMode/context";

function Header({ title, favoritesCount, cartItemsCount }) {
  const context = useContext(Context);

  return (
    <header className="header">
      <h1>
        <NavLink to="/" className="header-title">
          {title}
        </NavLink>
      </h1>
      <Button
        onClick={context.toggleMod}
        className={"open-btn table__mod-btn"}
        text={"Table Mod"}
      />
      <div className="svg-wrapper">
        <NavLink to="/favorites" className="active-link">
          <Favorite favoritesCount={favoritesCount} />
        </NavLink>
        <NavLink className="active-link" to="/cart">
          <Cart cartItemsCount={cartItemsCount} />
        </NavLink>
      </div>
    </header>
  );
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  favoritesCount: PropTypes.number.isRequired,
  cartItemsCount: PropTypes.number.isRequired,
};
export default Header;
