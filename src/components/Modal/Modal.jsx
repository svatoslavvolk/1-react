import PropTypes from "prop-types";
import React from 'react';

function Modal({ actionType, product, onConfirm, onClose }) {
    const handleWrapperClick = (e) => {
        if (e.target === e.currentTarget) {
            onClose();
        }
    };

    const buttonText = actionType === "delete" ? "Delete" : "Add to Cart";
    const actionText = actionType === "delete" ? "delete" : "add";

    return (
        <div className="modal-wrapper" onClick={handleWrapperClick}>
            <div className="modal">
                <p className="modal-text">
                    Are you sure you want  {actionText}
                    <span className="modal-span"> <br/> {product?.name} <br/> </span>
                    {actionType === "delete" ? "from" : "to"} the cart?
                </p>
                <button onClick={() => {
                    onConfirm(product);
                    onClose();
                }} className="btn add-btn">{buttonText}</button>
                <button onClick={onClose} className="btn close-btn">Close</button>
            </div>
        </div>

    );
}

Modal.propTypes = {
    product: PropTypes.object,
    onConfirm: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    name: PropTypes.string
}

Modal.defaultProps = {
    name: "this product"
}

export default Modal;