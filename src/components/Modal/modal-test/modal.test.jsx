import Modal from "../Modal";
import React from "react";

import { describe, it, expect} from 'vitest';

import {fireEvent, render} from "@testing-library/react";

describe('Modal component', () => {
    it('renders without crashing', () => {
        const { getByText } = render(<Modal isOpen={true} onConfirm={() => {}} onClose={() => {}} />);
        expect(getByText(/Are you sure you want add.*to the cart?/)).not.toBeNull();
    });

    it('calls onClose when close button is clicked', () => {
        const onCloseMock = vi.fn();
        const { getByText } = render(<Modal isOpen={true} onConfirm={() => {}} onClose={onCloseMock} />);

        const closeButton = getByText('Close');
        fireEvent.click(closeButton);

        expect(onCloseMock).toHaveBeenCalled();
    });

    it('renders correctly with snapshot', () => {
        const modal = render(<Modal isOpen={true} onConfirm={() => {}} onClose={() => {}} />);
        expect(modal).toMatchSnapshot();
    });n
});


