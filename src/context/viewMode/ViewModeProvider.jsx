import { Context } from './context'
import React, { useState, useEffect } from "react";

const ViewModeProvider = ({ children }) => {
    // Инициализация состояния с учетом значения из localStorage
    const [mode, setMode] = useState(() => {
        const savedMode = localStorage.getItem('viewMode');
        return savedMode ? savedMode : 'flex';
    });

    const toggleMod = () => {
        setMode(prevMod => {
            const newMode = prevMod === "grid" ? "" : "grid";
            localStorage.setItem('viewMode', newMode); // Сохранение в localStorage
            return newMode;
        });
    }

    // Этот эффект будет срабатывать при монтировании компонента, чтобы установить значение из localStorage.
    useEffect(() => {
        const savedMode = localStorage.getItem('viewMode');
        if (savedMode) {
            setMode(savedMode);
        }
    }, []);

    return <Context.Provider value={{ mode, toggleMod }}>{children}</Context.Provider>
}

export { ViewModeProvider }
