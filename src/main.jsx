import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App.jsx";
import {BrowserRouter} from "react-router-dom";
import store from "./store/store";
import {Provider} from "react-redux";
import {ViewModeProvider} from "./context/viewMode/ViewModeProvider";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <BrowserRouter>
        <ViewModeProvider>
            <Provider store={store}>
                <App/>
            </Provider>
        </ViewModeProvider>
    </BrowserRouter>
);
