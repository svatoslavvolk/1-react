const initialState = {
    products: [],
    isModalOpen: false
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_PRODUCTS':
            return {
                ...state,
                products: action.payload
            };
        case 'TOGGLE_MODAL':
            return {
                ...state,
                isModalOpen: action.payload
            };
        default:
            return state;
    }
};

export default rootReducer;