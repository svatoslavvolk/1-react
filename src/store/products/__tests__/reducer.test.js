import { productsReducer } from "../reducer";
import {
  ProductsActions,
  CartActions,
  ModalActions,
  FavoriteActions,
} from "../actions";
import { afterEach, describe, expect, test } from "vitest";

const initialState = {
  products: [],
  cart: [],
  favorites: [],
  isModalOpen: false,
  modalActionType: null,
  selectedProduct: null,
};

describe("reducer tests", () => {
  afterEach(() => {
    localStorage.clear();
  });

  test("return unchanged state when action is incorrect", () => {
    const action = {
      type: "",
    };

    const result = productsReducer(initialState, action);
    const expectedResult = {
      products: [],
      cart: [],
      favorites: [],
      isModalOpen: false,
      modalActionType: null,
      selectedProduct: null,
    };
    expect(result).toStrictEqual(expectedResult);
  });

  test("ProductsActions.SET", () => {
    const action = {
      type: ProductsActions.SET,
      payload: [{ article: "001", name: "Product 1", count: 0 }],
    };

    const result = productsReducer(initialState, action);
    expect(result.products).toStrictEqual(action.payload);
  });

  test("CartActions.ADD_ITEM", () => {
    const product = { article: "001", name: "Product 1", count: 0 };
    const stateWithProduct = { ...initialState, products: [product] };

    const action = {
      type: CartActions.ADD_ITEM,
      payload: "001",
    };

    const result = productsReducer(stateWithProduct, action);
    expect(result.cart).toStrictEqual([{ ...product, count: 1 }]);
  });

  test("CartActions.CLEAR", () => {
    const action = {
      type: CartActions.CLEAR,
    };

    const result = productsReducer(initialState, action);
    expect(result.cart).toStrictEqual([]);
  });

  test("CartActions.SET", () => {
    const products = [
      { article: "001", name: "Product 1", price: 100 },
      { article: "002", name: "Product 2", price: 150 },
    ];
    const initialStateWithProducts = { ...initialState, products };

    const action = {
      type: CartActions.SET,
      payload: ["001", "001", "002"],
    };

    const result = productsReducer(initialStateWithProducts, action);
    expect(result.cart).toStrictEqual([
      { ...products[0], count: 2 },
      { ...products[1], count: 1 },
    ]);
  });

  test("CartActions.REMOVE", () => {
    const productInCart = { article: "001", name: "Product 1", count: 1 };
    const stateWithProductInCart = { ...initialState, cart: [productInCart] };

    const action = {
      type: CartActions.REMOVE,
      payload: productInCart,
    };

    const result = productsReducer(stateWithProductInCart, action);
    expect(result.cart).toStrictEqual([]);
  });

  test("ModalActions.SET_IS_OPEN", () => {
    const action = {
      type: ModalActions.SET_IS_OPEN,
      payload: true,
    };

    const result = productsReducer(initialState, action);
    expect(result.isModalOpen).toBe(true);
  });

  test("ModalActions.SET_ACTION_TYPE", () => {
    const action = {
      type: ModalActions.SET_ACTION_TYPE,
      payload: "TEST_ACTION",
    };

    const result = productsReducer(initialState, action);
    expect(result.modalActionType).toBe("TEST_ACTION");
  });

  test("ModalActions.SET_SELECTED_PRODUCT", () => {
    const selectedProduct = { article: "001", name: "Product 1", count: 1 };

    const action = {
      type: ModalActions.SET_SELECTED_PRODUCT,
      payload: selectedProduct,
    };

    const result = productsReducer(initialState, action);
    expect(result.selectedProduct).toBe(selectedProduct);
  });

  test("FavoriteActions.SET", () => {
    const products = [
      { article: "001", name: "Product 1", price: 100 },
      { article: "002", name: "Product 2", price: 150 },
    ];
    const initialStateWithProducts = { ...initialState, products };

    const action = {
      type: FavoriteActions.SET,
      payload: ["001", "001", "002"],
    };

    const result = productsReducer(initialStateWithProducts, action);
    expect(result.favorites).toStrictEqual([
      { ...products[0], count: 2 },
      { ...products[1], count: 1 },
    ]);
  });

  test("FavoriteActions.REMOVE_FROM_FAVORITE", () => {
    const productInFavorites = { article: "001", name: "Product 1", count: 1 };
    const stateWithProductInFavorites = {
      ...initialState,
      favorites: [productInFavorites],
    };

    const action = {
      type: FavoriteActions.REMOVE_FROM_FAVORITE,
      payload: productInFavorites,
    };

    const result = productsReducer(stateWithProductInFavorites, action);
    expect(result.favorites).toStrictEqual([]);
  });
});
