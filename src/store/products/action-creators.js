import {
  CartActions,
  ModalActions,
  FavoriteActions,
  ProductsActions,
} from "./actions";

export const getProducts = () => async (dispatch) => {
  try {
    const products = await fetch("data.json");
    const productData = await products.json();
    dispatch(fetchProducts(productData));
  } catch (error) {
    console.log(error.message);
  }
};

export const loadCartProducts = () => async (dispatch) => {
  try {
    await dispatch(getProducts());
    const cartProductsArticles = JSON.parse(localStorage.getItem("cart"));
    dispatch(setCartItem(cartProductsArticles));
  } catch (error) {
    console.log(error.message);
  }
};

export const addItemToCart = (article) => {
  return {
    type: CartActions.ADD_ITEM,
    payload: article,
  };
};

export const deleteFromCart = (article) => (dispatch) => {
  const cartProductArticles = JSON.parse(localStorage.getItem("cart")) ?? [];
  const productIndex = cartProductArticles.findIndex(
    (productArticle) => productArticle === article
  );
  if (productIndex !== -1) {
    const newCartArticles = cartProductArticles.toSpliced(productIndex, 1);
    localStorage.setItem("cart", JSON.stringify(newCartArticles));
    dispatch(setCartItem(newCartArticles));
  }
};

export const toggleFavorites = (article) => (dispatch) => {
  const favoriteArticles =
    JSON.parse(localStorage.getItem("favoritesList")) ?? [];
  const isInFavorites = favoriteArticles.includes(article);
  let newFavoriteArticles = [...favoriteArticles];
  if (isInFavorites) {
    const productIndex = favoriteArticles.findIndex(
      (productArticle) => productArticle === article
    );
    if (productIndex !== -1) {
      newFavoriteArticles = favoriteArticles.toSpliced(productIndex, 1);
    }
  } else {
    newFavoriteArticles = [...new Set([...favoriteArticles, article])];
  }

  localStorage.setItem("favoritesList", JSON.stringify(newFavoriteArticles));
  dispatch(setFavorites(newFavoriteArticles));
};

export const loadFavoriteProducts = () => async (dispatch) => {
  try {
    await dispatch(getProducts());
    const favoriteProductsArticles = JSON.parse(
      localStorage.getItem("favoritesList")
    );
    dispatch(setFavorites(favoriteProductsArticles));
  } catch (error) {
    console.log(error.message);
  }
};

export const clearCart = () => ({
  type: CartActions.CLEAR,
});

export const removeFromCart = (itemArticle) => ({
  type: CartActions.REMOVE,
  payload: itemArticle,
});

export const fetchProducts = (products) => ({
  type: ProductsActions.SET,
  payload: products,
});

export const setCartItem = (cartItems) => ({
  type: CartActions.SET,
  payload: cartItems,
});

export const setFavorites = (favoriteItems) => ({
  type: FavoriteActions.SET,
  payload: favoriteItems,
});

export const removeFromFavorites = (itemArticle) => ({
  type: FavoriteActions.REMOVE,
  payload: itemArticle,
});

export const setIsModalOpen = (isModalOpen) => ({
  type: ModalActions.SET_IS_OPEN,
  payload: isModalOpen,
});

export const setModalActionType = (actionType) => ({
  type: ModalActions.SET_ACTION_TYPE,
  payload: actionType,
});

export const setSelectedProduct = (product) => ({
  type: ModalActions.SET_SELECTED_PRODUCT,
  payload: product,
});
