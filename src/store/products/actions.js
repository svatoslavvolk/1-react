const ProductsActions = {
  SET: "SET_PRODUCTS",
};

const CartActions = {
  CLEAR: "CLEAR_CART",
  SET: "SET_CART_ITEMS",
  REMOVE: "REMOVE_FROM_CART",
  ADD_ITEM: "ADD_ITEM",
};

const ModalActions = {
  SET_IS_OPEN: "SET_IS_MODAL_OPEN",
  SET_ACTION_TYPE: "SET_MODAL_ACTION_TYPE",
  SET_SELECTED_PRODUCT: "SET_SELECTED_PRODUCT",
};

const FavoriteActions = {
  REMOVE: "REMOVE_FROM_FAVORITE",
  SET: "SET_FAVORITES_ITEM",
};

export { CartActions, ModalActions, FavoriteActions, ProductsActions };
