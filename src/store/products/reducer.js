import {
  CartActions,
  ModalActions,
  FavoriteActions,
  ProductsActions,
} from "./actions";

const initialState = {
  products: [],
  cart: [],
  favorites: [],
  isModalOpen: false,
  modalActionType: null,
  selectedProduct: null,
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ProductsActions.SET:
      return {
        ...state,
        products: action.payload,
      };

    case CartActions.SET: {
      const cartProductsArticle = action.payload;
      const productsCount = cartProductsArticle.reduce(
        (acc, productArticle) => {
          acc[productArticle] = (acc[productArticle] || 0) + 1;
          return acc;
        },
        {}
      );

      const { products } = state;

      const filteredProducts = products.filter((product) =>
        cartProductsArticle.includes(product.article)
      );
      const cartProducts = filteredProducts.map((product) => ({
        ...product,
        count: productsCount[product.article],
      }));

      return {
        ...state,
        cart: cartProducts,
      };
    }

    case CartActions.ADD_ITEM: {
      const article = action.payload;
      const cartArticles = JSON.parse(localStorage.getItem("cart")) ?? [];
      const newCartArticles = [...cartArticles, article];

      localStorage.setItem("cart", JSON.stringify(newCartArticles));

      const { cart, products } = state;
      const cartItemIndex = cart.findIndex((item) => item.article === article);

      let newCart = [...cart];
      if (cartItemIndex > -1) {
        const cartItem = cart[cartItemIndex];

        newCart.splice(cartItemIndex, 1, {
          ...cartItem,
          count: cartItem.count + 1,
        });
      } else {
        const productItem = products.find((item) => item.article === article);

        newCart.push({ ...productItem, count: 1 });
      }

      return { ...state, cart: newCart };
    }

    case CartActions.CLEAR: {
      localStorage.removeItem("cart");
      return { ...state, cart: [] };
    }

    case CartActions.REMOVE: {
      const updatedCart = state.cart.filter((item) => item !== action.payload);
      localStorage.setItem("cart", JSON.stringify(updatedCart));
      return {
        ...state,
        cart: updatedCart,
      };
    }

    case ModalActions.SET_IS_OPEN: {
      return { ...state, isModalOpen: action.payload };
    }

    case ModalActions.SET_ACTION_TYPE: {
      return { ...state, modalActionType: action.payload };
    }

    case ModalActions.SET_SELECTED_PRODUCT: {
      return { ...state, selectedProduct: action.payload };
    }

    case FavoriteActions.SET: {
      const favoriteProductsArticle = action.payload;
      const favoriteCount = favoriteProductsArticle.reduce(
        (acc, productArticle) => {
          acc[productArticle] = (acc[productArticle] || 0) + 1;
          return acc;
        },
        {}
      );

      const { products } = state;

      const filteredProducts = products.filter((product) =>
        favoriteProductsArticle.includes(product.article)
      );
      const favoriteProducts = filteredProducts.map((product) => ({
        ...product,
        count: favoriteCount[product.article],
      }));

      return {
        ...state,
        favorites: favoriteProducts,
      };
    }

    case FavoriteActions.REMOVE:
      const updatedFavorites = state.favorites.filter(
        (item) => item !== action.payload
      );
      localStorage.setItem("favoritesList", JSON.stringify(updatedFavorites));
      return {
        ...state,
        favorites: updatedFavorites,
      };

    default:
      return state;
  }
};

export default productsReducer;
