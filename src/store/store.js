import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { productsReducer } from "./products/reducer";

const reducer = combineReducers({ products: productsReducer });

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;
